//
//  BGVideoLibary.h
//  BGVideoLibary
//
//  Created by Danny on 2020/7/7.
//  Copyright © 2020年 BGVideoLibary. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BGVideoLibary.
FOUNDATION_EXPORT double BGVideoLibaryVersionNumber;

//! Project version string for BGVideoLibary.
FOUNDATION_EXPORT const unsigned char BGVideoLibaryVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BGVideoLibary/PublicHeader.h>


#import <BGVideoLibary/BGVideoSdkController.h>
