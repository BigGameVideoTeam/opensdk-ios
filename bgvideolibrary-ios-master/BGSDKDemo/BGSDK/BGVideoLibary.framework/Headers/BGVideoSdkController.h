/****************************************************************************
 Copyright (c) 2010-2013 cocos2d-x.org
 Copyright (c) 2013-2016 Chukong Technologies Inc.
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
#import <UIKit/UIKit.h>

@class RootViewController;

@interface BGVideoSdkController : NSObject <UIApplicationDelegate>
{
}

@property(nonatomic, readonly) RootViewController* viewController;

-(void)BGVideoinit;
-(void)startCocosView:(UIViewController *)VC LogInMsg:(NSString*) msg ;// RootViewControl:(UIViewController*)RootVC_
//-(UIView*)startCocosView;


+(BGVideoSdkController *)GetInstance;
// 視訊狀況通知
+(void)registerVideoNotify; // 註冊通知
+(void)unregisterVideoNotify; // 反註冊通知
+(void)addVideoObserver:(nullable NSNotificationName)aName;
+(void)removeVideoObserver:(nullable NSNotificationName)aName;

+(void)toLeft;
+(void)toRight;
+(void)toUp;
+(void)toDown;

+(void)toScaleUp;
+(void)toScaleDown;
+(void)toScaleXUp;
+(void)toScaleXDown;
+(void)toScaleYUp;
+(void)toScaleYDown;

+(void)testUpdateVideo;

+(NSString*)GetVersion;
+(NSString*)GetAppID;
+(void)DownLoadNewApp:(NSString*)urlstring;


@end

