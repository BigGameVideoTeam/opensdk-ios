//
//  ViewController.m
//  BGSDKDemo
//
//  Created by Danny on 2019/7/10.
//  Copyright © 2019年 Danny. All rights reserved.
//

#import "ViewController.h"
#import <BGVideoLibary/BGVideoSdkController.h>
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[BGVideoSdkController GetInstance] BGVideoinit ]; //呼叫资源初始化
    // Do any additional setup after loading the view, typically from a nib.
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(BGSDKExitGame:)  name:@"BGSDKEXITEDGAME" object:nil];

}
- (IBAction)Btn1Click:(id)sender {
    //URL从API获取
    NSString *logmsg_=@"http://cdn.DEMO.com/pcH5/index.html?uid=105383813&token=DEMO06480785150F787DE82FCE1Febd9&sn=DEMO&account=T8194976&locale=zh_CN";
    //table参数参考对接文档
    //logmsg_ = logmsg_ + "&table=b06";
    [[BGVideoSdkController GetInstance] startCocosView:self LogInMsg:logmsg_ ];
}
- (IBAction)Btn2Click:(id)sender {
    //URL从API获取
    NSString *logmsg_=@"http://cdn.DEMO.com/pcH5/index.html?uid=105383813&token=DEMO06480785150F787DE82FCE1Febd9&sn=DEMO&account=T8194976&locale=zh_CN";
    //提示cui type 参数参考对接文档
    //logmsg_ = logmsg_ + "&cui=512&type=1567";
    [[BGVideoSdkController GetInstance] startCocosView:self LogInMsg:logmsg_ ];
}
-(void)BGSDKExitGame:(NSNotification *)notification{
    NSLog(@"退出SDK的回调");
}

@end
