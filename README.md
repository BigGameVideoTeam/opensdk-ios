# OpenSDK-ios

_version update 2022-11-15_ (2.6.2)

OpenSDK-ios 为BG真人娱乐ios平台SDK(支持iOS 9以上，arm64架构)，
该SDK包含百家乐、极速百家乐、共咪百家乐、多彩百家乐、龙虎、轮盘、骰宝、牛牛、炸金花等游戏。

对应兼容线上版本:1.x.x.x 以上

### 文档修改日志

修改日期 | 对应线上版本号 | 修改内容
----|------|----
2020-04-13 | 0.18.6.2    | 1.廣告頁與自定義加載頁功能兼容與修正
2020-04-16 | 0.18.6.3    | 1.修正热更新断点续传 2.增加全资源版本bundle(启动不必下载图像资源)
2020-07-24 | 0.19.0.0    | 1.引擎升级 2.逻辑优化调整 3.调整内部参数取用
2020-09-21 | 0.19.0.4    | 1.优化选线逻辑 2.逻辑优化调整
2020-10-20 | 0.19.0.8    | 1.修正IOS 14 Label描边失效
2021-03-25 | 0.19.1.9    | 1.SDK核心升级適配IOS 14 2.移除UIWebView 
2021-04-01 | 0.19.1.10   | 1.修正IOS14.4以上适应性问题 2.修正WebView在IPad平台判断
2021-07-16 | 0.19.2.0    | 1.支援多台视频流程 
2022-01-27 | 0.19.2.20   | 1.添加Swift SDK导入 2.支援Xcode13以上专案导入
2022-03-17 | 0.19.2.23   | 1.优化资源包加载方式 2.逻辑优化
2022-05-12 | 0.19.2.25   | 1.修正更新选线错误 2.优化资源包
2022-11-15 | 1.0.2.1     | 1.修正更新选线错误 2.优化资源包 3.bug修正

## 对接方式

## 1. 添加配置 (OC 与 Swift 相同)

(1) info.plist ： 使用到http的请求

    App Transport Security Settings     (Type:Dictionary)   
    -> Allow Arbitrary Loads (Type:Boolean Value:YES)

(2) Build Settings ： 由于framework的配置， 需要添加配置

    Build Options
    >Enable Bitcode          NO
    Linking 
    >Other Linker Flags    添加： -ObjC , -lc++


添加库

TARGETS->Build Phases->Link Binary With Libraries  
添加

    AVKit.framework
    WebKit.framework
    AVFoundation.framework
    AudioToolbox.framework
    CoreMotion.framework
    UIKit.framework
    JavaScriptCore.framework
    OpenGLES.framework
    OpenAL.framework
    MediaPlayer.framework
    libz.tbd
    libsqlite3.tbd
    libiconv.tbd
    SystemConfiguration.framework
    Security.framework
    CFNetwork.framework
    CoreMedia.framework

添加参数

TARGETS->Build Settings->Preprocessor Macros

添加

HAVE_INTTYPES_H HAVE_PKCRYPT HAVE_STDINT_H HAVE_WZAES HAVE_ZLIB $(inherited)

## 2.引入SDK (OC 与 Swift 相同)

相关文件在BGSDK 目录内，请前往查看。

需要把BGVideoLibary.framework、BGVideBundle.bundle、KSYMediaPlayer.framework 拖入工程。

TARGETS->build Phases->Embedded Framework

添加KSYMediaPlayer.framework

## 3. 代码接入SDK

### OC 导入： 

#### (1) 引入SDK文件：

    #import <BGVideoLibary/BGVideoSdkController.h>

#### (2) 呼叫资源初始化

    於viewDidLoad
    添加 [[BGVideoSdkController GetInstance] BGVideoinit ]; 呼叫资源初始化

    - (void)viewDidLoad {
        [super viewDidLoad];
        [[BGVideoSdkController GetInstance] BGVideoinit ];
    }

#### (3) 调用SDK方法
    self: 当前的ViewController
    LogInMsg: 登入信息
    [[BGVideoSdkController GetInstance] startCocosView:self LogInMsg:logmsg_ ];

#### (4) 监听SDK消息通知

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(BGSDKExitGame:)  name:@"BGSDKEXITEDGAME" object:nil];

    -(void)BGSDKExitGame:(NSNotification *)notification{

    NSLog(@"退出SDK的回调");

    }

### Swift 导入： 

#### (1) 引入SDK文件

    import BGVideoLibary

#### (2) 呼叫资源初始化

    於viewDidLoad
    添加 [[BGVideoSdkController GetInstance] BGVideoinit ]; 呼叫资源初始化

    override func viewDidLoad() {
        super.viewDidLoad()      
        //BGinit
        BGVideoSdkController.getInstance().bgVideoinit();
    }

#### (3) 调用SDK方法

    self: 当前的ViewController
    LogInMsg: 登入信息
    BGVideoSdkController.getInstance().startCocosView(self, logInMsg: loginMsg_)

#### (4) 监听SDK消息通知

    override func viewDidLoad() {
        super.viewDidLoad() 
        let notificationName = Notification.Name(rawValue: "BGSDKEXITEDGAME")
        NotificationCenter.default.addObserver(self,selector:#selector(BGSDKExitGame(notification:)),name: notificationName, object: nil)
    }

    @objc private func BGSDKExitGame(notification: Notification){
        NSLog("退出SDK的回调");
    }

    deinit {
        //移除通知监听
        NotificationCenter.default.removeObserver(self)
    }

## 4. 问题排除

### IPAD
Deployment Info 需勾选 Requires full screen关闭 split view